FROM openjdk:8

RUN apt-get update && apt-get install -y maven
COPY . /project
RUN  cd /project && mvn package
RUN ls

ENTRYPOINT ["java", "-jar", "/project/target/testapp-0.0.1-SNAPSHOT.jar"]
