CREATE TABLE requests(
	id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	date DATETIME,
	response_time LONG,
	status_code SMALLINT,
	request_type_id TINYINT(1)
);