CREATE TABLE items(
	item_id VARCHAR(20),
	title VARCHAR(255),
	category_id VARCHAR(20),
	price DOUBLE,
	start_time DATETIME,
	stop_time DATETIME,
	parent_item_id VARCHAR(20),
	PRIMARY KEY(item_id)
);