package com.meli.testapp.businesslogic;

import com.meli.testapp.dataaccess.datamodels.Item;
import com.meli.testapp.dataaccess.repositories.ItemRepository;
import com.meli.testapp.dataaccess.repositories.RequestRepository;
import com.meli.testapp.externalservices.meliapiservices.apiconsumers.ItemConsumer;
import com.meli.testapp.externalservices.meliapiservices.apimodels.ItemResponse;
import com.meli.testapp.middlewares.healthmanagement.datamodels.ApiRequest;
import com.meli.testapp.middlewares.modelmappers.ItemDAOToItemVM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Component
public class MeliItemGetter {

    @Autowired
    ItemRepository itemRepository;
    @Autowired
    RequestRepository requestRepository;
    @Autowired
    ItemConsumer itemConsumer;
    private Mono<Item> result;

    public MeliItemGetter() {

    }


    public Mono<com.meli.testapp.services.viewmodels.Item> getMeliItem(String id) {
        Item itemOnDatabase = itemRepository.findByItemId(id);
        ItemDAOToItemVM itemConverter = new ItemDAOToItemVM();
        if (itemOnDatabase != null) {
            System.out.println("======= hit en base, retorna");
            return Mono.just(itemConverter.convertItemDaoToItemVM(itemOnDatabase));
        } else {
            System.out.println("======= miss, guarda en base y retorna");
            Mono<Tuple2<Long, ItemResponse>> itemMono = itemConsumer.getItem(id);
            Mono<Tuple2<Long, ItemResponse[]>> childrenMono = itemConsumer.getChildren(id);

            Date requestDate = new Date();
            return itemMono.zipWith(childrenMono).map(value -> {
                ItemResponse itemResponse = value.getT1().getT2();
                ItemResponse[] childrenResponse = value.getT2().getT2();
                Set<Item> children = new HashSet<>();
                for (int i = 0; i < childrenResponse.length; i++) {
                    Item child = new Item(childrenResponse[i].id, childrenResponse[i].title, childrenResponse[i].category_id, childrenResponse[i].price, childrenResponse[i].start_time, childrenResponse[i].stop_time, null, itemResponse.id);
                    children.add(child);
                }

                Item item = new Item(itemResponse.id, itemResponse.title, itemResponse.category_id, itemResponse.price, itemResponse.start_time, itemResponse.stop_time, children, null);
                itemRepository.saveAll(children);
                itemRepository.save(item);

                Long itemElapsedTime = value.getT1().getT1();
                Long childrenElapsedTime = value.getT2().getT1();
                Set<ApiRequest> apiRequests = new HashSet<ApiRequest>() {{
                    add(new ApiRequest(requestDate, itemElapsedTime, HttpStatus.OK.value()));
                    add(new ApiRequest(requestDate, childrenElapsedTime, HttpStatus.OK.value()));
                }};
                requestRepository.saveAll(apiRequests);
                return itemConverter.convertItemDaoToItemVM(item);
            }).onErrorResume((error) -> {
                ((WebClientResponseException) error).getStatusCode();
                requestRepository.save(new ApiRequest(requestDate, 0, ((WebClientResponseException) error).getStatusCode().value())); //Obtener elapsed time
                return Mono.empty();
            });
        }
    }
}
