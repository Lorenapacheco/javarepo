package com.meli.testapp;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestappApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(TestappApplication.class, args);
	}

	@Override
	public void run(String... args) {
		System.out.println("Application running");
	}
}