package com.meli.testapp.externalservices.meliapiservices.apiconsumers;

import com.meli.testapp.externalservices.meliapiservices.apimodels.ItemResponse;
import com.meli.testapp.propertiesmanagement.MeliProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

@Component
public class ItemConsumer {

    @Autowired
    private ExternalServicesConsumer esConsumer;

    public Mono<Tuple2<Long, ItemResponse>> getItem(String itemId){
        MeliProperties meliProperties = new MeliProperties();
        String uri = meliProperties.getBaseUrl() +"/"+ meliProperties.getItemsEndpoint() + "/" + itemId;
        return esConsumer.get(ItemResponse.class, uri);
    }

    public Mono<Tuple2<Long, ItemResponse[]>> getChildren(String parentItemId){
        MeliProperties meliProperties = new MeliProperties();
        String uri = meliProperties.getBaseUrl() +"/"+ meliProperties.getItemsEndpoint() + "/" + parentItemId + "/" + meliProperties.getItemsChildrenEndpoint();
        return esConsumer.get(ItemResponse[].class, uri);
    }

}
