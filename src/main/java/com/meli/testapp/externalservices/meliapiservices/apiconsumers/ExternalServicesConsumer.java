package com.meli.testapp.externalservices.meliapiservices.apiconsumers;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

@Component
public class ExternalServicesConsumer<T> {

    public Mono<Tuple2<Long, T>> get(Class<T> tClass, String uri){
        return WebClient.create()
                .get()
                .uri(uri)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(tClass)
                .elapsed();
    }
}
