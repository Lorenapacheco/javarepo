package com.meli.testapp.externalservices.meliapiservices.apimodels;

import java.util.Date;

public class ItemResponse {
    public String id;
    public String title;
    public String category_id;
    public Double price;
    public Date start_time;
    public Date stop_time;
}
