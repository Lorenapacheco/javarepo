package com.meli.testapp.dataaccess.datamodels;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "items")
public class Item {
    @Id
    @Column(name = "item_id", length = 20)
    private String itemId;
    @Column(name = "title", nullable = true, length = 20)
    private String title;
    @Column(name = "category_id", nullable = true, length = 255)
    private String categoryId;
    @Column(name = "price")
    private Double price;
    @Column(name = "start_time")
    private Date startTime;
    @Column(name = "stop_time")
    private Date stopTime;
    @OneToMany
    @JoinColumn(name="parent_item_id")
    private Set<Item> children;
    @Column(name = "parent_item_id", length = 20)
    private String parentItemId;

    public Item() {

    }

    public Item(String itemId, String title, String categoryId, Double price, Date startTime, Date stopTime, Set<Item> children, String parentItemId) {
        this.itemId = itemId;
        this.title = title;
        this.categoryId = categoryId;
        this.price = price;
        this.startTime = startTime;
        this.stopTime = stopTime;
        this.children = children;
        this.parentItemId = parentItemId;
    }

    public String getId() {
        return this.itemId;
    }
    public String getTitle() {
        return this.title;
    }
    public String getCategoryId() {
        return this.categoryId;
    }
    public Double getPrice() {
        return this.price;
    }
    public Date getStartTime() {
        return this.startTime;
    }
    public Date getStopTime() {
        return this.stopTime;
    }
    public Set<Item> getChildren() {
        return this.children;
    }
}
