package com.meli.testapp.dataaccess.repositories;

import com.meli.testapp.middlewares.healthmanagement.datamodels.Request;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RequestRepository extends CrudRepository<Request, Long> {
}
