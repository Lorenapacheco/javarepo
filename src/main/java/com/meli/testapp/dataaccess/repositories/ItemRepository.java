package com.meli.testapp.dataaccess.repositories;

import com.meli.testapp.dataaccess.datamodels.Item;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemRepository extends CrudRepository<Item, Long> {
    public Item findByItemId(String id);
}
