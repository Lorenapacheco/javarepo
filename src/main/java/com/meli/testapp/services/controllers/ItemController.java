package com.meli.testapp.services.controllers;

import com.meli.testapp.businesslogic.MeliItemGetter;
import com.meli.testapp.services.viewmodels.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/items")
public class ItemController {

    @Autowired
    MeliItemGetter itemGetter;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<Item> findById(@PathVariable("id") String id) {
        //return RestPreconditions.checkFound(service.findById(id));
        return itemGetter.getMeliItem(id);
    }

}