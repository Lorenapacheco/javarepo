package com.meli.testapp.services.viewmodels;

public class ItemChild {
    private String item_id;
    private String stop_time;

    public ItemChild(String item_id, String stop_time){
        this.item_id = item_id;
        this.stop_time = stop_time;
    }

    public String getItem_id() {
        return this.item_id;
    }

    public String getStop_time(){
        return this.stop_time;
    }

}
