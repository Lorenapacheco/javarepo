package com.meli.testapp.services.viewmodels;

import java.util.Set;

public class Item {
    private String item_id;
    private String title;
    private String category_id;
    private Double price;
    private String start_time;
    private String stop_time;
    private Set<ItemChild> children;


    public Item(String itemId, String title, String categoryId, Double price, String startTime, String stopTime, Set<ItemChild> children) {
        this.item_id = itemId;
        this.title = title;
        this.category_id = categoryId;
        this.price = price;
        this.start_time = startTime;
        this.stop_time = stopTime;
        this.children = children;
    }

    public String getId() {
        return this.item_id;
    }

    public String getTitle() {
        return this.title;
    }

    public String getCategory_id() {
        return this.category_id;
    }

    public Double getPrice() {
        return this.price;
    }

    public String getStart_time() {
        return this.start_time;
    }

    public String getStop_time() {
        return this.stop_time;
    }

    public Set<ItemChild> getChildren() {
        return this.children;
    }

}
