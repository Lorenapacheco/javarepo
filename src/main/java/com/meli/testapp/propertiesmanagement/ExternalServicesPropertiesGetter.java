package com.meli.testapp.propertiesmanagement;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ExternalServicesPropertiesGetter {
    public String getProperty(String propertyName){
        String result = "";
        try (InputStream input = getClass().getResourceAsStream("/externalservices.properties")) {
            Properties prop = new Properties();
            prop.load(input);
            result = prop.getProperty(propertyName);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return result;
    }
}
