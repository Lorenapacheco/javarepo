package com.meli.testapp.propertiesmanagement;

public interface ExternalServiceProperties {
    String propertiesPrefix = "";
    ExternalServicesPropertiesGetter propertiesGetter = null;

    public String getBaseUrl();
}
