package com.meli.testapp.propertiesmanagement;

public class MeliProperties implements ExternalServiceProperties {
    private String propertiesPrefix = "meliServices";
    private ExternalServicesPropertiesGetter propertiesGetter;

    public MeliProperties(){
        this.propertiesGetter = new ExternalServicesPropertiesGetter();
    }

    public String getBaseUrl(){
        return this.propertiesGetter.getProperty(propertiesPrefix + ".baseUrl");
    }

    public String getItemsEndpoint(){
        return this.propertiesGetter.getProperty(propertiesPrefix + ".services.items");
    }

    public String getItemsChildrenEndpoint(){
        return this.propertiesGetter.getProperty(propertiesPrefix + ".services.items.children");
    }
}
