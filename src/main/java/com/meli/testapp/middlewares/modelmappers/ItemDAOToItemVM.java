package com.meli.testapp.middlewares.modelmappers;

import com.meli.testapp.dataaccess.datamodels.Item;
import com.meli.testapp.services.viewmodels.ItemChild;

import java.util.HashSet;
import java.util.Set;

public class ItemDAOToItemVM {
    public com.meli.testapp.services.viewmodels.Item convertItemDaoToItemVM(Item itemDao) {
        // falta conversion de formato de fecha
        Set<ItemChild> children = new HashSet<>();
        for (Item i : itemDao.getChildren()) {
            children.add(new ItemChild(i.getId(), i.getStopTime().toString()));
        }
        com.meli.testapp.services.viewmodels.Item itemVm = new com.meli.testapp.services.viewmodels.Item(itemDao.getId(),
                itemDao.getTitle(),
                itemDao.getCategoryId(),
                itemDao.getPrice(),
                itemDao.getStartTime().toString(),
                itemDao.getStopTime().toString(),
                children
        );
        return itemVm;
    }
}
