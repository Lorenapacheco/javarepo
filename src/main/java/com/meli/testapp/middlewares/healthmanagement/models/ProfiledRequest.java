package com.meli.testapp.middlewares.healthmanagement.models;

public class ProfiledRequest<T> {
    private T request;
    private long elapsedTime;

    public ProfiledRequest(T request, long elapsedTime){
        this.request = request;
        this.elapsedTime = elapsedTime;
    }

    public T getRequest(){
        return this.request;
    }

    public long getElapsedTime() {
        return elapsedTime;
    }
}
