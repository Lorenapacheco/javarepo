package com.meli.testapp.middlewares.healthmanagement.datamodels;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "requests")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
        discriminatorType = DiscriminatorType.INTEGER,
        name = "request_type_id",
        columnDefinition = "TINYINT(1)"
)
@DiscriminatorValue("1")
public class Request {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "date", length = 20)
    private Date date;
    @Column(name = "response_time", length = 20)
    private long responseTime;

    public Request(){}

    public Request(Date date, Long responseTime){
        this.date = date;
        this.responseTime = responseTime;
    }

    public Date getDate(){
        return this.date;
    }
    public long getResponseTime(){
        return this.responseTime;
    }
}
