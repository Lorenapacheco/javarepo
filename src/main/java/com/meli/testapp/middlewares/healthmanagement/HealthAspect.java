package com.meli.testapp.middlewares.healthmanagement;

import com.meli.testapp.dataaccess.datamodels.Item;
import com.meli.testapp.dataaccess.repositories.RequestRepository;
import com.meli.testapp.middlewares.healthmanagement.models.ProfiledRequest;
import com.meli.testapp.middlewares.healthmanagement.datamodels.Request;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.Date;

@Component
@Aspect
public class HealthAspect {

    @Autowired
    RequestRepository requestRepository;

    //Aspect para interceptar pedidos recibidos en controllers
    @Around("within(com.meli.testapp.services.controllers..*)")
    public Mono<Item> doAppResponseProfiling(ProceedingJoinPoint pjp) throws Throwable {
        Date date = new Date();
        Profiler<Mono<Item>> profiler = new Profiler<>();
        ProfiledRequest profiledRequest = profiler.profileCall(pjp);
        Request request = new Request(date, profiledRequest.getElapsedTime());
        requestRepository.save(request);
        return (Mono<Item>)profiledRequest.getRequest();
    }
/*
    //Aspect para interceptar pedidos a APIs externas - descartado por uso de elapsed de WebClient
    @After("execution(* com.meli.testapp.externalservices.meliapiservices.apiconsumers.ExternalServicesConsumer.*(..))")
    public Mono<Tuple2<Long, Object>> doApiRequestProfiling(ProceedingJoinPoint pjp) throws Throwable {
        Date date = new Date();
        Mono<Tuple2<Long, Object>> apiResponse = (Mono<Tuple2<Long, Object>>)pjp.proceed();
        ApiRequest request = new ApiRequest(date, apiResponse);
        requestRepository.save(request);
        return (Mono<ItemResponse>)profiledRequest.getRequest();
    }
*/
}
