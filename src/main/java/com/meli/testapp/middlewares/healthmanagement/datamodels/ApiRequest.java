package com.meli.testapp.middlewares.healthmanagement.datamodels;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Date;

@Entity
@DiscriminatorValue("2")
public class ApiRequest extends Request{

    @Column(name = "status_code", length = 20)
    private Integer statusCode;

    public ApiRequest(){}

    public ApiRequest(Date date, long responseTime, Integer statusCode){
        super(date, responseTime);
        this.statusCode = statusCode;
    }

    public Integer getStatusCode(){
        return this.statusCode;
    }
}
