package com.meli.testapp.middlewares.healthmanagement;

import com.meli.testapp.middlewares.healthmanagement.models.ProfiledRequest;
import org.aspectj.lang.ProceedingJoinPoint;

public class Profiler<T> {

    public ProfiledRequest profileCall(ProceedingJoinPoint pjp) throws Throwable{
        long start = System.currentTimeMillis();
        T requestResult = (T)pjp.proceed();
        long ellapsedTime = System.currentTimeMillis();
        return new ProfiledRequest(requestResult, ellapsedTime - start);
    }
}
